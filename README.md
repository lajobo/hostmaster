# Hostmaster

This project shows a selection of games which can interactively be played e.g. on a projector on a party.
It was designed in a way, so that it could easily be reused for different events and allows the user to configure the games (at least a bit) on the fly.
While at the same time, it was never meant to be maintained or even continued or extended at all.
Therefore the code might look quick and dirty written in many places.

**Important**: Since this project was mainly build for one specific event, it might be worthy to check out the branch `feature/laura` instead,
since that branch contains some adjustments/fixes for some games to make it a better experience and is overall more "polished".

## Quick start

```bash
npm install
```
followed by
```bash
npm run start
```

Then open [http://localhost:3000/](http://localhost:3000/) to see the application.

## Usage
To understand the concept of the party, the project was built for, it might help to read the [Trivia](#trivia) first.

### Configuration

When you start the application for the first time, you will need to enter the name of the student and its responsible supervisor.
All the data you will configure beforehand will be saved in the local storage.
Due to that, it's possible to configure everything beforehand and just start it directly during the event itself.
As long as you have access to the computer that will be used.

After that you will see the different games and the scoreboard, each with two buttons, and a button to reset all configuration.
Most of the games need some kind of configuration before it can be played, for that you just click the **Configure** button and fill the shown fields which should be more or less obvious.
Each configuration requires a dropdown which will decide, who will play the game. That's important to show the correct labels and distribute the points correctly in the scoreboard.

#### Music Quiz
The Music Quiz can be played in up to 5 matches. For each match you have to paste the id of a Youtube video which will be played during the game.
So if the Youtube url looks like this https://www.youtube.com/watch?v=dQw4w9WgXcQ, you have to copy the part at the end after the = sign, in this case `dQw4w9WgXcQ`.
#### Pigpen Cipher
The Pigpen Cipher can be played in up to 5 matches. For each match you have to write down a string that needs to be decrypted.
It can consist of upper- and lowercase characters, those letters will automatically be encrypted for the game. All other characters will be shown decrypted.
#### Arrow Path
The Arrow Path can be played in up to 5 matches. For each match you have to write down a string that needs to be figured out.
It can consist of uppercase letters only, while certain characters are blocked (it's not possible to enter them), since they are not representable, like the letter Q for example.
#### Multiple Choice
The Arrow Path can be played in up to 10 matches. For each match you have to write down a string that contains the question, the correct answer and a bunch of wrong answers.
It needs to follow the structure that is shown above the inputs. For example like this:

`What is the capital of Sweden?;Stockholm;Malmö;Copenhagen;Kalmar`

The correct answer needs to be the first one, followed by the wrong answers. During the game, the answers will be shuffled everytime you start the game.

### Play

To play a game, you just need to click the **Play** button under the game name.
If you haven't configured the game yet, you will automatically be redirected to the configuration page, otherwise the game will open directly.

Despite the game, the header will look more or less the same for each game. In the center you can see the name of the current game.

Underneath of that you can see the competitors names, depending on what you have configured earlier on.
By `control + left click` on a competitor name, you can increase the score. While `control + shift + left click` will decrease it.

In the upper right corner you will see two buttons. The first one is to go **Back to menu**. The other one either states **Next round** or **Scoreboard**.
If you have configured multiple rounds and have not reached the last round, it will show **Next round**, otherwise **Scoreboard**.
When you `control + left click` on this button, it will go back to the previous round, if there was any.
After all matches are played, you will be redirected to the **Scoreboard**, when you click the Scoreboard button.
It will automatically add the game name and the score to it.

#### Treasure Map
The Treasure Map is a game, where a map is shown and the host of the event will tell a story.
Based on the story, the competitors will need to figure out, where the treasure is located.
This game is well suited to be played together with the audience.
Since it is very specific to the event, the map needs to be changed for every event.
By `control + left click` on the map, it will switch to an alternative version of the map, which can be used to show some hints.

#### Memory
The Memory game is pretty simple and well known. It's a memory game where you reveal two cards and have to find the matching ones.
If the cards are the same, they will stay unrevealed, and you can continue. Otherwise, they will reveal again and the other player continues.

#### Music Quiz
In the Music Quiz you will see 4 control buttons and a `Resolve` button.
First, you have to click the `Play` button, which will start to play the music of the video only.
With the `<<<` and `>>>` controls you can jump 10 seconds forward or backward and with the `|<` button you can go back to the beginning.
Once the competitors have guessed the song, you can show the video by clicking the `Resolve` button.

#### Pigpen Cipher
The Pigpen cipher is a geometric simple substitution cipher. Check [Wikipedia](https://en.wikipedia.org/wiki/Pigpen_cipher) for more information.

In the game you will see the encrypted string that needs to be decrypted.
Underneath you see a cheat sheet which shows an incomplete mapping of the characters.
By `control + left click` on the cheat sheet it will switch to a complete version of it.
To reveal the search string, you can `control + left` click on it as well.

#### Arrow Path
The Arrow Path game shows you a dotted grid and a series of arrows underneath.
You have to start at the dot in the bottom right and draw a line by following the direction of the arrows. This long line will create a string at the end.
When you `control + left click` on the dotted grid, an animation will start which shows you the solution of the path.

#### Multiple Choice
In the Multiple Choice game you will see the question together with the answers underneath. Each answer will have a radio button on the left and right to select the answer for each competitor.
When you click the `Reveal` button, it will lock the answers, color the correct answer green, color the incorrect answers red and automatically give each player with a correct answer 1 point.

#### Scoreboard
The scoreboard can be used between the games to either change/fix/extend the points by clicking on the `Configure` button, or it can just be shown again by clicking the `Show` button.

## Design
During the implementation it was pretty unclear under which conditions the project will need to run later on.
It was only clear that it will run on a projector at a party and for this reason it will require big font sizes, big buttons and big images so that everyone will be able to see it.
To build it fully responsive and resizeable to all possible screen sizes and ratios was considered unnecessary for this project.
Furthermore, it should be controllable by mouse and keyboard and be able to revert changes, to have a "professional" presentation during the final event.

## Extensibility
The whole project was solely build for one specific event and hence was never planned to be continued or to consist of nice code.
It was built quick and dirty while still being clean enough to have a comfortable time implementing it.
But it's neither meant to be continued nor extended. Due to that, the project is considered done and closed.

## Trivia
In Kalmar in Sweden it's usual, that during the dissertation party there will be a bunch of games between the student and her/his supervisor.
Those games are not meant 100% serious and should somehow be related to the student and her/his hobbies.
Anyhow, the student is always meant to win the evening in general.
It should show, that the student became the master and is finally smarter than the teacher/supervisor.
To achieve that, it is even sometimes common to cheat, help the student, distract the supervisor or similar.
Often it is even combined with gifts that the student will receive for each game that was won.
Due to that, it can be even more important to cheat, so that the student will win each game for sure.

#### Author

* [Lars Bomnüter](mailto:larsbomnueter@web.de)

---

MIT License
