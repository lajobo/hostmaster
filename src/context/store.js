import React from "react";

export const StoreContext = React.createContext({ state: undefined, dispatch: undefined });
