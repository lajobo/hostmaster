import {actionTypes} from "../enums/store";

export const setNames = (student, supervisor) => ({
    type: actionTypes.SET_NAMES,
    payload: {
        student,
        supervisor
    }
});

export const reset = () => ({
    type: actionTypes.RESET
});

export const setPigpenCipherConfig = (name, text) => ({
    type: actionTypes.SET_PIGPEN_CIPHER_CONFIG,
    payload: {
        name,
        text
    }
});

export const setArrowPathConfig = (name, text) => ({
    type: actionTypes.SET_ARROW_PATH_CONFIG,
    payload: {
        name,
        text
    }
});

export const setMusicQuizConfig = (name, text) => ({
    type: actionTypes.SET_MUSIC_QUIZ_CONFIG,
    payload: {
        name,
        text
    }
});

export const setMultipleChoiceConfig = (name, text) => ({
    type: actionTypes.SET_MULTIPLE_CHOICE_CONFIG,
    payload: {
        name,
        text
    }
});

export const setCompetitors = (game, competitors) => ({
    type: actionTypes.SET_COMPETITORS,
    payload: {
        game,
        competitors
    }
});

export const setScore = (game, score, competitors) => ({
    type: actionTypes.SET_SCORE,
    payload: {
        game,
        score,
        competitors
    }
});

export const deleteScore = (index) => ({
    type: actionTypes.DELETE_SCORE,
    payload: {
        index
    }
});
