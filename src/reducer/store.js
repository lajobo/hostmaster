import {actionTypes} from "../enums/store";

export const storeReducer = (state, action) => {
    switch (action.type) {
        case actionTypes.SET_NAMES:
            return {
                ...state,
                names: {
                    student: action?.payload?.student || "",
                    supervisor: action?.payload?.supervisor || "",
                }
            }
        case actionTypes.RESET:
            return {};
        case actionTypes.SET_PIGPEN_CIPHER_CONFIG:
            return {
                ...state,
                pigpenCipherConfig: {
                    ...state?.pigpenCipherConfig,
                    [action?.payload?.name]: action?.payload?.text
                }
            };
        case actionTypes.SET_ARROW_PATH_CONFIG:
            return {
                ...state,
                arrowPathConfig: {
                    ...state?.arrowPathConfig,
                    [action?.payload?.name]: action?.payload?.text
                }
            };
        case actionTypes.SET_MUSIC_QUIZ_CONFIG:
            return {
                ...state,
                musicQuizConfig: {
                    ...state?.musicQuizConfig,
                    [action?.payload?.name]: action?.payload?.text
                }
            };
        case actionTypes.SET_MULTIPLE_CHOICE_CONFIG:
            return {
                ...state,
                multipleChoiceConfig: {
                    ...state?.multipleChoiceConfig,
                    [action?.payload?.name]: action?.payload?.text
                }
            };
        case actionTypes.SET_COMPETITORS:
            return {
                ...state,
                competitors: {
                    ...state?.competitors,
                    [action?.payload?.game]: action?.payload?.competitors
                }
            };
        case actionTypes.SET_SCORE:
            let score;

            switch (action?.payload?.competitors) {
                case "all":
                    score = action?.payload?.score;
                    break;
                case "student_audience":
                    score = [
                        action?.payload?.score?.[0] || 0,
                        0,
                        action?.payload?.score?.[1] || 0
                    ];
                    break;
                case "supervisor_audience":
                    score = [
                        0,
                        action?.payload?.score?.[0] || 0,
                        action?.payload?.score?.[1] || 0
                    ];
                    break;
                default:
                    score = [
                        action?.payload?.score?.[0] || 0,
                        action?.payload?.score?.[1] || 0,
                        0
                    ];
                    break;
            }

            return {
                ...state,
                score: [
                    ...(state?.score || []),
                    {
                        game: action?.payload?.game,
                        score
                    }
                ]
            };
        case actionTypes.DELETE_SCORE:
            return {
                ...state,
                score: state.score.filter((value, index) => index !== action?.payload?.index)
            }
        default:
            throw new Error();
    }
}
