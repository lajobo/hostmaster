import "../../style/pigpenCipherAlphabet.css";
import {useEffect, useState} from "react";

export const PigpenCipherAlphabet = (props) => {
    const [mode, setMode] = useState("hard");

    useEffect(() => {
        setMode("hard");
    }, [props.roundsCurrent]);

    return (
        <img
            onClick={(event) => {
                if (event.ctrlKey) {
                    setMode((currentMode) => currentMode === "easy" ? "hard" : "easy");
                }
            }}
            className={"pigpen-cipher-alphabet"}
            alt={"pigpen-cipher-alphabet"}
            src={"img/pigpenCipher/" + mode + ".png"}
        />
    );
}
