import "../../style/pigpenCipherGame.css";
import {useContext, useEffect, useState} from "react";
import {StoreContext} from "../../context/store";

export const PigpenCipherGame = (props) => {
    const { state } = useContext(StoreContext);
    const [resolved, setResolved] = useState(false);

    useEffect(() => {
        setResolved(false);
    }, [props.roundsCurrent]);

    return (
        <div
            className={"pigpen-cipher-game" + (resolved ? "" : " pigpen")}
            onClick={(event) => {
                if (event.ctrlKey) {
                    setResolved((currentResolved) => !currentResolved);
                }
            }}
        >
            {Object.values(state?.pigpenCipherConfig)[props.roundsCurrent]}
        </div>
    );
}
