import "../../style/arrowPathPath.css";

const characterArrowMap = new Map([
    ["A", "WWDXADX"],
    ["B", "WWDXADXAD"],
    ["C", "WWDAXXD"],
    ["D", "WWCYD"],
    ["E", "WWDAXDAXD"],
    ["F", "WWDAXDAXD"],
    ["H", "WWXDWXX"],
    ["I", "WWXX"],
    ["J", "WXDWWADXX"],
    ["K", "WWXEYC"],
    ["L", "WWXXD"],
    ["M", "WWCEXX"],
    ["N", "WWCCWWXX"],
    ["O", "WWDXXAD"],
    ["P", "WWDXAXXD"],
    ["R", "WWDXAC"],
    ["S", "DWAWDAXDX"],
    ["T", "DWWADDAXXD"],
    ["U", "WWXXDWWXX"],
    ["V", "DDQQCCEEYYDD"],
    ["W", "DDQQCCECEEYYDD"],
    ["X", "EQCEYC"],
    ["Y", "DWQCEYXD"],
    ["Z", "EEAADDYYDD"],
    [" ", ""],
    ["NEXT", "D"]
]);

const textToArrows = (text) => {
    const characters = (text || "").split("");
    return characters.map((character, index) => {
        const isLast = (index + 1) >= characters.length;
        return [
            (characterArrowMap.has(character) ? characterArrowMap.get(character) : ""),
            (isLast ? "" : characterArrowMap.get("NEXT"))
        ].join("");
    });
}

export const ArrowPathPath = (props) => {
    return (
        <div className={"arrow-path-path arrow"}>
            {textToArrows(props.text)}
        </div>
    );
}
