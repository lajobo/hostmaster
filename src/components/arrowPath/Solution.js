import "../../style/arrowPathSolution.css";
import {useEffect, useRef, useState} from "react";
import {drawPolygon, generateVertices, relativeToAbsoluteVertices} from "../../utils/canvas";

export const ArrowPathSolution = (props) => {
    const canvas = useRef();
    const [reveal, setReveal] = useState(false);
    const [hide, setHide] = useState(false);
    const [width, setWidth] = useState(0);

    useEffect(() => {
        setWidth(window.innerWidth * 0.8);
    }, []);

    useEffect(() => {
        setHide((currentHide) => !currentHide);
        setTimeout(() => {
            setHide((currentHide) => !currentHide);
        }, 10);
    }, [props.text])

    useEffect(() => {
        if (canvas?.current) {
            const ctx = canvas.current.getContext("2d");
            ctx.scale(2, 2);

            if (reveal) {
                let vertices = relativeToAbsoluteVertices(generateVertices(props.text));
                ctx.strokeStyle = "#333";

                ctx.lineCap = "round";
                ctx.lineWidth = 4;

                drawPolygon(ctx, vertices, 25);
            } else {
                ctx.clearRect(0,0,5000,1000);
                setHide((currentHide) => !currentHide);
                setTimeout(() => {
                    setHide((currentHide) => !currentHide);
                }, 10);
            }

        }
    }, [props.text, canvas, reveal]);

    return hide ? null : (
        <canvas
            width={Math.ceil(width)}
            height={"340"}
            ref={canvas}
            className={"arrow-path-solution"}
            onClick={(event) => {
                if (event.ctrlKey) {
                    setReveal((currentReveal) => !currentReveal);
                }
            }}
        />
    );
}
