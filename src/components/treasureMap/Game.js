import "../../style/treasureMapGame.css";
import {useState} from "react";

export const TreasureMapGame = () => {
    const [show, setShow] = useState(false);

    return (
        <img
            className={"treasure-map-game"}
            alt={""}
            src={"img/treasureMap/" + (show ? "resolved" : "hidden") + ".png"}
            onClick={(event) => {
                if (event.ctrlKey) {
                    setShow((currentShow) => !currentShow);
                }
            }}
        />
    );
}
