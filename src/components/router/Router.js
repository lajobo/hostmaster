import {Routes} from "react-router";
import {Route} from "react-router-dom";
import {MenuPage} from "../../pages/Menu";
import {ArrowPathPage} from "../../pages/ArrowPath";
import {ArrowPathConfigPage} from "../../pages/ArrowPathConfig";
import {MemoryPage} from "../../pages/Memory";
import {MemoryConfigPage} from "../../pages/MemoryConfig";
import {PigpenCipherPage} from "../../pages/PigpenCipher";
import {PigpenCipherConfigPage} from "../../pages/PigpenCipherConfig";
import {ScoreboardPage} from "../../pages/Scoreboard";
import {ScoreboardConfigPage} from "../../pages/ScoreboardConfig";
import {MusicQuizPage} from "../../pages/MusicQuiz";
import {MusicQuizConfigPage} from "../../pages/MusicQuizConfig";
import {MultipleChoicePage} from "../../pages/MultipleChoice";
import {MultipleChoiceConfigPage} from "../../pages/MultipleChoiceConfig";
import {TreasureMapConfigPage} from "../../pages/TreasureMapConfig";
import {TreasureMapPage} from "../../pages/TreasureMap";

export const Router = () => {
    return (
        <Routes>
            <Route path="/" element={<MenuPage />} />
            <Route path="treasure-map" element={<TreasureMapPage />} />
            <Route path="treasure-map/config" element={<TreasureMapConfigPage />} />
            <Route path="pigpen-cipher" element={<PigpenCipherPage />} />
            <Route path="pigpen-cipher/config" element={<PigpenCipherConfigPage />} />
            <Route path="arrow-path" element={<ArrowPathPage />} />
            <Route path="arrow-path/config" element={<ArrowPathConfigPage />} />
            <Route path="memory" element={<MemoryPage />} />
            <Route path="memory/config" element={<MemoryConfigPage />} />
            <Route path="multiple-choice" element={<MultipleChoicePage />} />
            <Route path="multiple-choice/config" element={<MultipleChoiceConfigPage />} />
            <Route path="music-quiz" element={<MusicQuizPage />} />
            <Route path="music-quiz/config" element={<MusicQuizConfigPage />} />
            <Route path="scoreboard" element={<ScoreboardPage />} />
            <Route path="scoreboard/config" element={<ScoreboardConfigPage />} />
        </Routes>
    );
};
