import "../../style/namesDisplay.css";
import {useContext} from "react";
import {StoreContext} from "../../context/store";
import {useI18n} from "../../hooks/useI18n";

export const NamesDisplay = () => {
    const i18n = useI18n();
    const { state } = useContext(StoreContext);

    return (
        <div className={"names-display"}>
            <div>
                <span>{i18n?.names?.student}:</span>
                <span>{state?.names?.student}</span>
            </div>
            <div>
                <span>{i18n?.names?.supervisor}:</span>
                <span>{state?.names?.supervisor}</span>
            </div>
        </div>
    );
}
