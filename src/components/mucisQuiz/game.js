import "../../style/musicQuizGame.css";
import YouTube from "react-youtube";
import {useState} from "react";
import {Button} from "../button/Button";

export const MusicQuizGame = (props) => {
    const [youtube, setYoutube] = useState();
    const [paused, setPaused] = useState(true);
    const [show, setShow] = useState(false);
    const opts = {
        height: "390",
        width: "640",
        playerVars: {
            autoplay: 1,
        },
    };

    const onReady = (event) => {
        setYoutube(event);
        event.target.pauseVideo();
    }

    const playPause = () => {
        if (youtube) {
            if (paused) {
                youtube.target.playVideo();
                setPaused(false);
            } else {
                youtube.target.pauseVideo();
                setPaused(true);
            }

        }
    }

    const reset = () => {
        if (youtube) {
            youtube.target.seekTo(0);
        }
    }

    const back = () => {
        if (youtube) {
            const currentTime = youtube.target.getCurrentTime();
            const targetTime = Math.max(currentTime - 10, 0);
            youtube.target.seekTo(targetTime);
        }
    }

    const forward = () => {
        if (youtube) {
            const currentTime = youtube.target.getCurrentTime();
            const durationTime = youtube.target.getDuration();
            const targetTime = Math.min(currentTime + 10, durationTime);
            youtube.target.seekTo(targetTime);
        }
    }

    const resolve = () => {
        setShow((currentShow) => !currentShow);
    }

    return (
        <div className={["music-video-quiz", show ? "show" : ""].join(" ")}>
            <div className={"row player"}>
                <YouTube videoId={props?.youtubeId} opts={opts} onReady={onReady} />
            </div>
            <div className={"row"}>
                <Button onClick={reset}>{"|<"}</Button>
                <Button onClick={back}>{"<<<"}</Button>
                <Button onClick={playPause}>{paused ? "Play" : "Pause"}</Button>
                <Button onClick={forward}>{">>>"}</Button>
            </div>
            <div className={"row"}>
                <Button onClick={resolve}>{"Resolve"}</Button>
            </div>
        </div>
    );
}
