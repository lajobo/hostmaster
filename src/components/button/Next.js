import "../../style/buttonNext.css";
import {useI18n} from "../../hooks/useI18n";
import {Button} from "./Button";
import {Link} from "react-router-dom";

export const NextButton = (props) => {
    const i18n = useI18n();
    const lastRound = (props.roundsCurrent + 1) >= props.roundsTotal;

    const button = (
        <Button className={"next-button"} onClick={props.onNextRound}>
            {i18n?.components?.button?.next?.[lastRound ? "titleLast" : "title"]}
        </Button>
    );

    return lastRound
        ? <Link to={"/scoreboard"}>{button}</Link>
        : button;
}
