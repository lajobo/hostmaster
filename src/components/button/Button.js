import "../../style/button.css";

export const Button = (props) => {
    return (
        <div className={["button", props.className].filter(Boolean).join(" ")}>
            <button type={props.type} onClick={props?.onClick}>
                {props.children}
            </button>
        </div>
    );
};
