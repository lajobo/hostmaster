import "../../style/buttonBack.css";
import {useI18n} from "../../hooks/useI18n";
import {Link} from "react-router-dom";
import {Button} from "./Button";

export const BackButton = () => {
    const i18n = useI18n();

    return (
        <Link className={"back-button"} to={"/"}>
            <Button>
                {i18n?.components?.button?.back?.title}
            </Button>
        </Link>
    );
}
