import "../../style/scoreboardResultTemplate.css";
import {Button} from "../button/Button";
import {useI18n} from "../../hooks/useI18n";

export const ScoreboardResultTemplate = (props) => {
    const i18n = useI18n();

    return (
        <div className={["scoreboard-result-template", props?.className, props.delete ? "delete" : ""].join(" ")}>
            <div className={"id"}>{props.id}</div>
            <div className={"name"}>{props.name}</div>
            <div className={"student"}>{props.student}</div>
            {props.combineSupervisorAudience
                ? (
                    <div className={"supervisor-audience"}>{props.supervisorAudience}</div>
                )
                : (
                    <>
                        <div className={"supervisor"}>{props.supervisor}</div>
                        <div className={"audience"}>{props.audience}</div>
                    </>
                )}
            {(props.delete || props.onDelete) && (
                <div className={"delete"}>
                    {props.delete
                        ? props.delete
                        : (
                            <Button
                                type={"delete"}
                                onClick={() => {
                                    if (props.onDelete) {
                                        props.onDelete();
                                    }
                                }}
                            >
                                {i18n?.components?.scoreboardResultTemplate?.delete}
                            </Button>
                        )
                    }
                </div>
            )}
        </div>
    );
}
