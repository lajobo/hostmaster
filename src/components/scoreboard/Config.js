import "../../style/scoreboardConfig.css";
import {useContext, useState} from "react";
import {Button} from "../button/Button";
import {useI18n} from "../../hooks/useI18n";
import {setScore} from "../../actions/store";
import {StoreContext} from "../../context/store";

export const ScoreboardConfig = () => {
    const i18n = useI18n();
    const { dispatch } = useContext(StoreContext);
    const [formData, setFormData] = useState({ game: "", score: [0, 0, 0] });

    const onChange = (name, key) => (event) => {
        if (name === "game") {
            setFormData({
                ...formData,
                [name]: event.target.value
            });
        } else {
            const newScore = [...formData.score];
            newScore[key] = Number(event.target.value);

            setFormData({
                ...formData,
                [name]: newScore
            });
        }
    };

    return (
        <form className={"scoreboard-config"} onSubmit={(event) => {
            event.preventDefault();
            dispatch(setScore(formData.game, formData.score, "all"));
            setFormData({ game: "", score: [0, 0, 0] });
        }}>
            <div className={"id"}>
                <Button type={"submit"}>{i18n?.components?.scoreboardConfig?.add}</Button>
            </div>
            <div className={"name"}>
                <input required type={"text"} name={"game"} value={formData.game} onChange={onChange("game")} />
            </div>
            <div className={"student"}>
                <input required type={"number"} name={"score"} value={formData.score[0]} onChange={onChange("score", 0)} />
            </div>
            <div className={"supervisor"}>
                <input required type={"number"} name={"score"} value={formData.score[1]} onChange={onChange("score", 1)} />
            </div>
            <div className={"audience"}>
                <input required type={"number"} name={"score"} value={formData.score[2]} onChange={onChange("score", 2)} />
            </div>
        </form>
    );
}
