import "../../style/scoreboardTotalResult.css";
import {ScoreboardResultTemplate} from "./ResultTemplate";
import {useContext} from "react";
import {StoreContext} from "../../context/store";
import {useI18n} from "../../hooks/useI18n";

export const ScoreboardTotalResult = (props) => {
    const i18n = useI18n();
    const { state } = useContext(StoreContext);
    const result = (state?.score || []).reduce((total, score) => {
        return [
            total[0] + score?.score?.[0],
            total[1] + score?.score?.[1] + score?.score?.[2],
        ];
    }, [0, 0]);

    return (
        <ScoreboardResultTemplate
            className={"scoreboard-total-result"}
            combineSupervisorAudience
            name={i18n?.components?.scoreboardTotalResult?.name}
            student={result[0]}
            supervisorAudience={result[1]}
            delete={props.showDelete}
        />
    );
}
