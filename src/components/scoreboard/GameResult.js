import {ScoreboardResultTemplate} from "./ResultTemplate";

export const ScoreboardGameResult = (props) => {
    return (
        <ScoreboardResultTemplate
            className={"scoreboard-game-result"}
            id={props?.id}
            name={props?.name}
            student={props?.student}
            supervisor={props?.supervisor}
            audience={props?.audience}
            delete={props.delete}
            onDelete={props.onDelete}
        />
    );
}
