import "../../style/scoreboardNames.css";
import {ScoreboardResultTemplate} from "./ResultTemplate";
import {useI18n} from "../../hooks/useI18n";
import {useContext} from "react";
import {StoreContext} from "../../context/store";

export const ScoreboardNames = (props) => {
    const i18n = useI18n();
    const { state } = useContext(StoreContext);

    return (
        <ScoreboardResultTemplate
            className={"scoreboard-names"}
            id={i18n?.components?.scoreboardNames?.id}
            name={i18n?.components?.scoreboardNames?.name}
            student={state?.names?.student}
            supervisor={state?.names?.supervisor}
            audience={i18n?.components?.scoreboardNames?.audience}
            delete={props.showDelete ? i18n?.components?.scoreboardNames?.delete : undefined}
        />
    );
}
