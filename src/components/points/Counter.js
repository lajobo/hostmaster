import "../../style/pointsCounter.css";
import {useI18n} from "../../hooks/useI18n";
import {useContext, useEffect, useState} from "react";
import {StoreContext} from "../../context/store";

const resolveCompetitorNames = (
    competitors,
    names,
    audience
) => {
    switch (competitors) {
        case "student_audience":
            return [names?.student, audience];
        case "supervisor_audience":
            return [names?.supervisor, audience];
        default:
            return [names?.student, names?.supervisor];
    }
}

export const PointsCounter = (props) => {
    const i18n = useI18n();
    const { state } = useContext(StoreContext);
    const [points, setPoints] = useState([0, 0]);
    const [competitorNames] = useState(resolveCompetitorNames(
        state?.competitors?.[props?.game],
        state?.names,
        i18n?.components?.points?.counter?.audience
    ));

    useEffect(() => {
        if (props.onPointsChange) {
            props.onPointsChange(points);
        }
    }, [points, props?.onPointsChange]);

    return (
        <div className={"points-counter"}>
            <div className={"player"}>
                <h3 onClick={(event) => {
                    if (event.ctrlKey) {
                        if (props?.onClick) {
                            props.onClick(event, 0);
                        } else {
                            setPoints([points[0] + (event.shiftKey ? -1 : 1), points[1]]);
                        }
                    }
                }}>{competitorNames[0]}</h3>
                <h4>{props?.value ? props?.value?.[0] : points[0]}</h4>
            </div>
            <div className={"player"}>
                <h3 onClick={(event) => {
                    if (event.ctrlKey) {
                        if (props?.onClick) {
                            props.onClick(event, 1);
                        } else {
                            setPoints([points[0], points[1] + (event.shiftKey ? -1 : 1)]);
                        }
                    }
                }}>{competitorNames[1]}</h3>
                <h4>{props?.value ? props?.value?.[1] : points[1]}</h4>
            </div>
        </div>
    );
}
