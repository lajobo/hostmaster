import {Link} from "react-router-dom";
import {useI18n} from "../../hooks/useI18n";
import {Button} from "../button/Button";

export const MenuButtonGroup = (props) => {
    const i18n = useI18n();
    return (
        <div className={"button-group"}>
            <h2>{props.title}</h2>
            <div className={"container"}>
                <Link to={props.url}>
                    <Button>{props?.i18n?.play || i18n?.menu?.play}</Button>
                </Link>
                <Link to={props.url + "/config"}>
                    <Button>{i18n?.menu?.configure}</Button>
                </Link>
            </div>
        </div>
    );
};
