import "../../style/competitorSelection.css";
import {useI18n} from "../../hooks/useI18n";
import {useContext} from "react";
import {StoreContext} from "../../context/store";
import {setCompetitors} from "../../actions/store";

export const CompetitorSelection = (props) => {
    const i18n = useI18n();
    const { state, dispatch } = useContext(StoreContext);

    return (
        <div className={"competitor-selection"}>
            <form>
                <select value={state.competitors?.[props?.game] || ""} onChange={(event) => {
                    dispatch(setCompetitors(props?.game, event.target.value));
                }}>
                    <option value={""} disabled>
                        {""}
                    </option>
                    <option value={"student_supervisor"}>
                        {state.names.student + " " + i18n.components.competitor.selection.vs + " " + state.names.supervisor}
                    </option>
                    <option value={"student_audience"}>
                        {state.names.student + " " + i18n.components.competitor.selection.vs + " " + i18n.components.competitor.selection.audience}
                    </option>
                    <option value={"supervisor_audience"}>
                        {state.names.supervisor + " " + i18n.components.competitor.selection.vs + " " + i18n.components.competitor.selection.audience}
                    </option>
                </select>
            </form>
        </div>
    );
}
