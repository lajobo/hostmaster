import "../../style/configDescription.css";

export const ConfigDescription = (props) => {
    return (
        <div className={"config-description"}>
            {props.description}
        </div>
    );
}
