import {useI18n} from "../../hooks/useI18n";

export const AdminHeader = (props) => {
    const i18n = useI18n();

    return (
        <>
            <h1>{props?.title || i18n?.app?.title}</h1>
        </>
    )
}
