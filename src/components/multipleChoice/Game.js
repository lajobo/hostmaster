import "../../style/multipleChoiceGame.css";
import {useEffect, useState} from "react";
import {Button} from "../button/Button";
import {useI18n} from "../../hooks/useI18n";

const parseConfig = (configString) => {
    const config = (configString || "").split(";");
    const question = config.shift();
    const correctAnswerString = config?.[0];
    const answers = config
        .map((value) => ({ value, sort: Math.random() }))
        .sort((a, b) => a.sort - b.sort)
        .map(({ value }) => value);
    const correctAnswerIndex = answers.findIndex((answer) => answer === correctAnswerString);

    return {
        question,
        answers,
        correctAnswerIndex
    };
};

export const MultipleChoiceGame = (props) => {
    const i18n = useI18n();
    const [config, setConfig] = useState(parseConfig(props.config));
    const [revealed, setRevealed] = useState(false);
    const [selection, setSelection] = useState([undefined, undefined]);

    useEffect(() => {
        setRevealed(false);
        setSelection([undefined, undefined]);
        setConfig(parseConfig(props.config));
    }, [props?.config]);

    return (
        <div className={"multiple-choice-game"}>
            <h2 className={"question"}>
                {config.question}
            </h2>
            <form className={["answers", revealed ? "revealed" : ""].join(" ")}>
                {config.answers.map((answer, index) => {
                    return (
                        <div key={answer + index} className={["row", config?.correctAnswerIndex === index ? "correct" : "wrong"].join(" ")}>
                            <label className={"competitor"}>
                                <input
                                    type={"radio"}
                                    name={"competitor-1"}
                                    checked={selection?.[0] === index}
                                    onChange={() => {
                                        if (!revealed) {
                                            setSelection((currentSelection) => [index, currentSelection?.[1]]);
                                        }
                                    }}
                                />
                            </label>
                            <div className={"answer"}>
                                {answer}
                            </div>
                            <label className={"competitor"}>
                                <input
                                    type={"radio"}
                                    name={"competitor-2"}
                                    checked={selection?.[1] === index}
                                    onChange={() => {
                                        if (!revealed) {
                                            setSelection((currentSelection) => [currentSelection?.[0], index]);
                                        }
                                    }}
                                />
                            </label>
                        </div>
                    );
                })}
            </form>
            <Button
                className={"reveal"}
                onClick={() => {
                    if (selection.filter((currentSelection) => currentSelection !== undefined).length === 2) {
                        setRevealed((currentRevealed) => !currentRevealed);

                        if (props?.onReveal) {
                            props.onReveal([
                                selection?.[0] === config?.correctAnswerIndex ? (revealed ? -1 : 1) : 0,
                                selection?.[1] === config?.correctAnswerIndex ? (revealed ? -1 : 1) : 0,
                            ]);
                        }
                    }
                }}
            >
                {i18n?.components?.multipleChoice?.game?.[revealed ? "hide" : "reveal"]}
            </Button>
        </div>
    );
}
