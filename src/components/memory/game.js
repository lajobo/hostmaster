import "../../style/memoryGame.css";
import {useState} from "react";

const generateMemoryGrid = (width, height) => {
    const itemAmount = (width * height) / 2;
    let items = [...Array(itemAmount)].reduce((itemsResult, _, itemIndex) => {
        return [...itemsResult, itemIndex + 1, itemIndex + 1];
    }, []);
    let index;
    let item;

    return [...Array(height)].map((row, rowIndex) => {
        return [...Array(width)].map((col, colIndex) => {
            index = Math.floor(Math.random() * items.length);
            item = items[index];
            delete items[index];
            items = items.filter(n => n);
            return { nr: item, show: false, rowIndex, colIndex };
        });
    });
}

export const MemoryGame = () => {
    const [grid, setGrid] = useState(generateMemoryGrid(5, 4));
    const [firstItem, setFirstItem] = useState();
    const [noClick, setNoClick] = useState(false);

    const onClick = (rowIndex, colIndex) => (event) => {
        if (event.shiftKey) {
            setGrid(generateMemoryGrid(5, 4));
        } else {
            if (
                noClick ||
                (firstItem && firstItem.rowIndex === rowIndex && firstItem.colIndex === colIndex) ||
                grid[rowIndex][colIndex].show
            ) {
                return;
            }

            const newGrid = JSON.parse(JSON.stringify(grid));
            newGrid[rowIndex][colIndex] = {
                ...newGrid[rowIndex][colIndex],
                show: true
            };
            setGrid(newGrid);

            if (!firstItem) {
                setFirstItem(grid[rowIndex][colIndex]);
            } else {
                if (firstItem.nr !== grid[rowIndex][colIndex].nr) {
                    setNoClick(true);
                    setTimeout(() => {
                        const newGrid = JSON.parse(JSON.stringify(grid));
                        newGrid[rowIndex][colIndex] = {
                            ...newGrid[rowIndex][colIndex],
                            show: false
                        };
                        newGrid[firstItem.rowIndex][firstItem.colIndex] = {
                            ...newGrid[firstItem.rowIndex][firstItem.colIndex],
                            show: false
                        };


                        setNoClick(false);
                        setFirstItem();
                        setGrid(newGrid);
                    }, 1000);
                } else {
                    setFirstItem();
                }
            }
        }
    }

    return (
        <div className={"memory-game"}>
            {grid.map((row, rowIndex) => {
                return (
                    <div key={rowIndex} className={"row"}>
                        {row.map((item, colIndex) => {
                            return (
                                <div key={colIndex} className={"item"} onClick={onClick(rowIndex, colIndex)}>
                                    <img
                                        alt={""}
                                        src={item.show ? "img/memory/" + item.nr + ".png" : "img/memory/background.png"}
                                    />
                                </div>
                            );
                        })}
                    </div>
                );
            })}
        </div>
    );
};
