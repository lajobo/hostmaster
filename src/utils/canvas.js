const getVectorFromTwoPoints = (point1, point2) => ({
    x: point2.x - point1.x,
    y: point2.y - point1.y,
});

const getDistanceBetweenPoints = (point1, point2) => {
    const x = point1.x - point2.x;
    const y = point1.y - point2.y;

    return Math.sqrt(x * x + y * y);
};

const getTime = typeof performance === "function" ? performance.now : Date.now;

const drawLine = (ctx, startPoint, endPoint, drawingSpeed = 5, onAnimationEnd, startingLength = 0) => {
    let lastUpdate = getTime();

    // Set initial state
    let currentPoint = startPoint;
    const vector = getVectorFromTwoPoints(startPoint, endPoint);
    const startToEndDistance = getDistanceBetweenPoints(startPoint, endPoint);

    const lineStep = (drawingSpeed * 1000) / startToEndDistance;

    const vectorStep = {
        x: vector.x * lineStep,
        y: vector.y * lineStep,
    };

    const animate = () => {
        const now = getTime();
        const delta = (now - lastUpdate) / 1000 / 60; // 60fps frame duration ~16.66ms;

        const deltaVector = {
            x: vectorStep.x * delta,
            y: vectorStep.y * delta,
        };

        // Add starting length if any
        if (startingLength) {
            const startingLengthFactor = startingLength / startToEndDistance;

            deltaVector.x += vector.x * startingLengthFactor;
            deltaVector.y += vector.y * startingLengthFactor;

            // We've drawn it once, we don't want to draw it again
            startingLength = 0;
        }

        // Set next point
        let nextPoint = {
            x: currentPoint.x + deltaVector.x,
            y: currentPoint.y + deltaVector.y
        };

        let newStartingLength = 0;
        let isFinished = false;

        const startToNextPointDistance = getDistanceBetweenPoints(startPoint, nextPoint);

        // The next point is past the end point
        if (startToNextPointDistance >= startToEndDistance) {
            newStartingLength = startToNextPointDistance - startToEndDistance;
            isFinished = true;
            nextPoint = endPoint;
        }

        // Draw line segment
        ctx.beginPath();
        ctx.moveTo(currentPoint.x, currentPoint.y);
        ctx.lineTo(nextPoint.x, nextPoint.y);
        ctx.stroke();

        if (isFinished) {
            if (onAnimationEnd) {
                onAnimationEnd(newStartingLength);
            }
            return;
        }

        // Move current point to the end of the drawn segment
        currentPoint = nextPoint;

        // Update last updated time
        lastUpdate = now;

        // Store requestAnimationFrame ID so we can cancel it
        requestAnimationFrame(animate);
    };

    // Start animation
    animate();
};

export const drawPolygon = (ctx, vertices, drawingSpeed = 5, onAnimationEnd, startingLength = 0, startingPointIndex = 0) => {
    const start = vertices[startingPointIndex];
    const end = vertices[startingPointIndex + 1];

    if (startingPointIndex + 1 >= vertices.length) {
        if (onAnimationEnd) {
            onAnimationEnd();
        }
        return;
    }

    drawLine(ctx, start, end, drawingSpeed, (startingLength) => {
        const newIndex = startingPointIndex + 1;

        drawPolygon(ctx, vertices, drawingSpeed, onAnimationEnd, startingLength, newIndex)
    }, startingLength);
};

const characterArrowMap = new Map([
    [
        "A",
        [
            { x: 0, y: -70},
            { x: 20, y: 0},
            { x: 0, y: 33},
            { x: -18, y: 0},
            { x: 0, y: 4},
            { x: 18, y: 0},
            { x: 0, y: 33, last: "A"}
        ]
    ],
    [
        "B",
        [
            { x: 0, y: -70},
            { x: 20, y: 0},
            { x: 0, y: 33},
            { x: -18, y: 0},
            { x: 0, y: 4},
            { x: 18, y: 0},
            { x: 0, y: 29},
            { x: -18, y: 0},
            { x: 0, y: 4},
            { x: 18, y: 0, last: "B"}
        ]
    ],
    [
        "C",
        [
            { x: 0, y: -70},
            { x: 20, y: 0},
            { x: 0, y: 4},
            { x: -16, y: 0},
            { x: 0, y: 66},
            { x: 16, y: 0, last: "C"}
        ]
    ],
    [
        "D",
        [
            { x: 0, y: -70},
            { x: 20, y: 0},
            { x: 20, y: 35},
            { x: -20, y: 31},
            { x: -20, y: 0},
            { x: 0, y: 4},
            { x: 40, y: 0, last: "D"}
        ]
    ],
    [
        "E",
        [
            { x: 0, y: -70},
            { x: 20, y: 0},
            { x: 0, y: 4},
            { x: -16, y: 0},
            { x: 0, y: 29},
            { x: 16, y: 0},
            { x: 0, y: 4},
            { x: -16, y: 0},
            { x: 0, y: 33},
            { x: 16, y: 0, last: "E"},
        ]
    ],
    [
        "F",
        [
            { x: 0, y: -70},
            { x: 20, y: 0},
            { x: 0, y: 4},
            { x: -16, y: 0},
            { x: 0, y: 29},
            { x: 16, y: 0},
            { x: 0, y: 4},
            { x: -16, y: 0},
            { x: 0, y: 33},
            { x: 16, y: 0, last: "F"},
        ]
    ],
    [
        "H",
        [
            { x: 0, y: -70},
            { x: 4, y: 0},
            { x: 0, y: 35},
            { x: 12, y: 0},
            { x: 0, y: -35},
            { x: 4, y: 0},
            { x: 0, y: 70, last: "H"},
        ]
    ],
    [
        "I",
        [
            { x: 0, y: -70},
            { x: 4, y: 0},
            { x: 0, y: 70},
            { x: -4, y: 0, last: "I"},
        ]
    ],
    [
        "J",
        [
            { x: 0, y: -35},
            { x: 4, y: 0},
            { x: 0, y: 35},
            { x: 12, y: 0},
            { x: 0, y: -66},
            { x: -16, y: 0},
            { x: 0, y: -4},
            { x: 20, y: 0},
            { x: 0, y: 70, last: "J"},
        ]
    ],
    [
        "K",
        [
            { x: 0, y: -70},
            { x: 4, y: 0},
            { x: 0, y: 33},
            { x: 12, y: -33},
            { x: 4, y: 0},
            { x: -12, y: 35},
            { x: 12, y: 35, last: "K"},
        ]
    ],
    [
        "L",
        [
            { x: 0, y: -70},
            { x: 4, y: 0},
            { x: 0, y: 70},
            { x: 16, y: 0, last: "L"},
        ]
    ],
    [
        "M",
        [
            { x: 0, y: -70},
            { x: 20, y: 35},
            { x: 20, y: -35},
            { x: 0, y: 70, last: "M"},
        ]
    ],
    [
        "N",
        [
            { x: 0, y: -70},
            { x: 36, y: 70},
            { x: 0, y: -70},
            { x: 4, y: 0},
            { x: 0, y: 70, last: "N"},
        ]
    ],
    [
        "O",
        [
            { x: 0, y: -70},
            { x: 20, y: 0},
            { x: 0, y: 66},
            { x: -16, y: 0},
            { x: 0, y: 4},
            { x: 16, y: 0, last: "O"},
        ]
    ],
    [
        "P",
        [
            { x: 0, y: -70},
            { x: 20, y: 0},
            { x: 0, y: 35},
            { x: -16, y: 0},
            { x: 0, y: 35},
            { x: 16, y: 0, last: "P"},
        ]
    ],
    [
        "R",
        [
            { x: 0, y: -70},
            { x: 20, y: 0},
            { x: 0, y: 35},
            { x: -16, y: 0},
            { x: 16, y: 35, last: "R"},
        ]
    ],
    [
        "S",
        [
            { x: 16, y: 0},
            { x: 0, y: -33},
            { x: -16, y: 0},
            { x: 0, y: -37},
            { x: 20, y: 0},
            { x: 0, y: 4},
            { x: -16, y: 0},
            { x: 0, y: 33},
            { x: 16, y: 0},
            { x: 0, y: 33, last: "S"},
        ]
    ],
    [
        "T",
        [
            { x: 18, y: 0},
            { x: 0, y: -66},
            { x: -18, y: 0},
            { x: 0, y: -4},
            { x: 40, y: 0},
            { x: 0, y: 4},
            { x: -18, y: 0},
            { x: 0, y: 66},
            { x: 18, y: 0, last: "T"},
        ]
    ],
    [
        "U",
        [
            { x: 0, y: -70},
            { x: 4, y: 0},
            { x: 0, y: 70},
            { x: 12, y: 0},
            { x: 0, y: -70},
            { x: 4, y: 0},
            { x: 0, y: 70, last: "U"},
        ]
    ],
    [
        "V",
        [
            { x: 38, y: 0},
            { x: -38, y: -70},
            { x: 4, y: 0},
            { x: 36, y: 66},
            { x: 36, y: -66},
            { x: 4, y: 0},
            { x: -40, y: 70},
            { x: 40, y: 0, last: "V"},
        ]
    ],
    [
        "W",
        [
            { x: 38, y: 0},
            { x: -38, y: -70},
            { x: 4, y: 0},
            { x: 36, y: 66},
            { x: 20, y: -35},
            { x: 20, y: 35},
            { x: 36, y: -66},
            { x: 4, y: 0},
            { x: -40, y: 70},
            { x: 40, y: 0, last: "W"},
        ]
    ],
    [
        "X",
        [
            { x: 18, y: -35},
            { x: -18, y: -35},
            { x: 4, y: 0},
            { x: 16, y: 33},
            { x: 16, y: -33},
            { x: 4, y: 0},
            { x: -18, y: 35},
            { x: 18, y: 35, last: "X"},
        ]
    ],
    [
        "Y",
        [
            { x: 18, y: 0},
            { x: 0, y: -35},
            { x: -18, y: -35},
            { x: 4, y: 0},
            { x: 16, y: 33},
            { x: 16, y: -33},
            { x: 4, y: 0},
            { x: -18, y: 35},
            { x: 0, y: 35},
            { x: 18, y: 0, last: "Y"},
        ]
    ],
    [
        "Z",
        [
            { x: 36, y: -66},
            { x: -36, y: 0},
            { x: 0, y: -4},
            { x: 40, y: 0},
            { x: -36, y: 70},
            { x: 36, y: 0, last: "Z"},
        ]
    ],
    [
        " ",
        [
            { x: 20, y: 0, last: "SPACE"},
        ]
    ],
    [
        "NEXT",
        [
            { x: 20, y: 0, last: "NEXT"},
        ]
    ]
]);

export const generateVertices = (text = "") => {
    const characters = text.split("");
    return characters.reduce(
        (path, character, index) => [
            ...path,
            ...(characterArrowMap.has(character) ? characterArrowMap.get(character) : []),
            ...((index + 1) < characters.length ? characterArrowMap.get("NEXT") : [])
        ],
        []
    );
}

export const relativeToAbsoluteVertices = (vertices, startPoint = { x: 30, y: 123 }) => {
    let lastPoint = startPoint;
    let nextPoint;
    return vertices.reduce((absoluteVertices, vertex) => {
        nextPoint = { ...vertex, x: lastPoint.x + vertex.x, y: lastPoint.y + vertex.y };
        lastPoint = nextPoint;
        return [...absoluteVertices, nextPoint];
    }, [startPoint]);
}
