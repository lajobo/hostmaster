import './style/app.css';
import React, {useEffect, useReducer} from "react";
import {storeReducer} from "./reducer/store";
import {NamesPage} from "./pages/Names";
import {StoreContext} from "./context/store";
import {Router} from "./components/router/Router";

export const App = () => {
    const [state, dispatch] = useReducer(storeReducer, JSON.parse(localStorage.getItem("state")) || {}, undefined);

    useEffect(() => {
        localStorage.setItem("state", JSON.stringify(state));
    }, [state]);

    return (
        <StoreContext.Provider value={{ state, dispatch }}>
            {!state?.names?.student ? <NamesPage /> : <Router />}
        </StoreContext.Provider>
    );
}
