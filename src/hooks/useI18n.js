import en from "../i18n/en.json";

export const useI18n = () => {
    return en;
};
