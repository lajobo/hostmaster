import {AdminHeader} from "../components/header/Admin";
import {useI18n} from "../hooks/useI18n";
import {useContext, useState} from "react";
import {StoreContext} from "../context/store";
import {BackButton} from "../components/button/Back";
import {PointsCounter} from "../components/points/Counter";
import {NextButton} from "../components/button/Next";
import {setScore} from "../actions/store";
import {TreasureMapGame} from "../components/treasureMap/Game";

export const TreasureMapPage = () => {
    const i18n = useI18n();
    const { state, dispatch } = useContext(StoreContext);
    const [points, setPoints] = useState([0, 0]);

    return (
        <div id={"treasure-map"}  className={"page"}>
            <AdminHeader title={i18n?.treasureMap?.title} />
            <BackButton />
            <NextButton
                roundsTotal={1}
                roundsCurrent={0}
                onNextRound={() => {
                    dispatch(setScore(i18n?.treasureMap?.title, points, state?.competitors?.treasureMap))
                }}
            />
            <PointsCounter game={"treasureMap"} onPointsChange={setPoints} />
            <TreasureMapGame />
        </div>
    );
};
