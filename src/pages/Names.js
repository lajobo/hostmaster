import '../style/names.css';
import {useI18n} from "../hooks/useI18n";
import {useContext, useState} from "react";
import {StoreContext} from "../context/store";
import {setNames} from "../actions/store";
import {AdminHeader} from "../components/header/Admin";
import {Button} from "../components/button/Button";

export const NamesPage = () => {
    const i18n = useI18n();
    const { dispatch } = useContext(StoreContext);
    const [formData, setFormData] = useState({ student: "", supervisor: "" });
    const onChange = (name) => (event) => {
        setFormData({
            ...formData,
            [name]: event.target.value
        });
    };

    return (
        <div id={"names"} className={"page"}>
            <AdminHeader />
            <form onSubmit={(event) => {
                event.preventDefault();
                if (formData?.student && formData?.supervisor) {
                    dispatch(setNames(formData?.student, formData?.supervisor));
                }
            }}>
                <div>
                    <label htmlFor={"student"}>{i18n?.names?.student}</label>
                    <input id={"student"} name={"student"} value={formData?.student} onChange={onChange("student")} required />
                </div>
                <div>
                    <label htmlFor={"supervisor"}>{i18n?.names?.supervisor}</label>
                    <input id={"supervisor"} name={"supervisor"} value={formData?.supervisor} onChange={onChange("supervisor")} required />
                </div>
                <Button type={"submit"}>{i18n?.names?.buttonSave}</Button>
            </form>
        </div>
    );
};
