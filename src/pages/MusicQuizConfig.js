import "../style/musicQuizConfig.css";
import {useI18n} from "../hooks/useI18n";
import {useContext} from "react";
import {StoreContext} from "../context/store";
import {AdminHeader} from "../components/header/Admin";
import {NamesDisplay} from "../components/names/Display";
import {BackButton} from "../components/button/Back";
import {CompetitorSelection} from "../components/competitor/Selection";
import {setMusicQuizConfig} from "../actions/store";

export const MusicQuizConfigPage = () => {
    const i18n = useI18n();
    const { state, dispatch } = useContext(StoreContext);

    const onChange = (name) => (event) => {
        dispatch(setMusicQuizConfig(name, event.target.value));
    }

    return (
        <div id={"music-quiz-config"} className={"page"}>
            <AdminHeader title={i18n?.musicQuizConfig?.title} />
            <NamesDisplay />
            <BackButton />
            <CompetitorSelection game={"musicQuiz"} />
            <div className={"match"}>
                <label htmlFor={"match1"}>
                    {i18n?.musicQuizConfig?.match1}
                </label>
                <input id={"match1"} value={state?.musicQuizConfig?.match1 || ""} onChange={onChange("match1")} />
            </div>
            <div className={"match"}>
                <label htmlFor={"match2"}>
                    {i18n?.musicQuizConfig?.match2}
                </label>
                <input id={"match2"} value={state?.musicQuizConfig?.match2 || ""} onChange={onChange("match2")} />
            </div>
            <div className={"match"}>
                <label htmlFor={"match3"}>
                    {i18n?.musicQuizConfig?.match3}
                </label>
                <input id={"match3"} value={state?.musicQuizConfig?.match3 || ""} onChange={onChange("match3")} />
            </div>
            <div className={"match"}>
                <label htmlFor={"match4"}>
                    {i18n?.musicQuizConfig?.match4}
                </label>
                <input id={"match4"} value={state?.musicQuizConfig?.match4 || ""} onChange={onChange("match4")} />
            </div>
            <div className={"match"}>
                <label htmlFor={"match5"}>
                    {i18n?.musicQuizConfig?.match5}
                </label>
                <input id={"match5"} value={state?.musicQuizConfig?.match5 || ""} onChange={onChange("match5")} />
            </div>
        </div>
    );
};
