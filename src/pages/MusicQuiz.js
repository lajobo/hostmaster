import {useI18n} from "../hooks/useI18n";
import {useContext, useState} from "react";
import {StoreContext} from "../context/store";
import {Navigate} from "react-router";
import {AdminHeader} from "../components/header/Admin";
import {BackButton} from "../components/button/Back";
import {NextButton} from "../components/button/Next";
import {setScore} from "../actions/store";
import {PointsCounter} from "../components/points/Counter";
import {MusicQuizGame} from "../components/mucisQuiz/game";

export const MusicQuizPage = () => {
    const i18n = useI18n();
    const { state, dispatch } = useContext(StoreContext);
    const roundsTotal = Object.keys(state?.musicQuizConfig || {}).length;
    const [roundsCurrent, setRoundsCurrent] = useState(0);
    const [points, setPoints] = useState([0, 0]);

    return roundsTotal === 0 ? <Navigate to={"config"} /> : (
        <div id={"music-quiz"}  className={"page"}>
            <AdminHeader title={i18n?.musicQuiz?.title} />
            <BackButton />
            <NextButton
                roundsTotal={roundsTotal}
                roundsCurrent={roundsCurrent}
                onNextRound={(event) => {
                    if (event.ctrlKey) {
                        setRoundsCurrent(roundsCurrent > 0 ? roundsCurrent - 1 : 0);
                    } else {
                        if ((roundsCurrent + 1) < roundsTotal) {
                            setRoundsCurrent(roundsCurrent + 1);
                        } else {
                            dispatch(setScore(i18n?.musicQuiz?.title, points, state?.competitors?.musicQuiz))
                        }
                    }
                }}
            />
            <PointsCounter game={"musicQuiz"} onPointsChange={setPoints} />
            <MusicQuizGame key={roundsCurrent} youtubeId={Object.values(state?.musicQuizConfig)?.[roundsCurrent]} />
        </div>
    );
};
