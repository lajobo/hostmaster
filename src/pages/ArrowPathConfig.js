import "../style/arrowPathConfig.css";
import {useI18n} from "../hooks/useI18n";
import {useContext} from "react";
import {StoreContext} from "../context/store";
import {AdminHeader} from "../components/header/Admin";
import {NamesDisplay} from "../components/names/Display";
import {BackButton} from "../components/button/Back";
import {setArrowPathConfig} from "../actions/store";
import {CompetitorSelection} from "../components/competitor/Selection";

const characterWhitelist = "abcdefhijklmnoprstuvwxyz ";
const characterWhitelistRegEx = new RegExp("^[" + characterWhitelist.toUpperCase().split("").join("|") + "]+$");

export const ArrowPathConfigPage = () => {
    const i18n = useI18n();
    const { state, dispatch } = useContext(StoreContext);

    const onChange = (name) => (event) => {
        if (characterWhitelistRegEx.test((event.target.value || "").toUpperCase())) {
            dispatch(setArrowPathConfig(name, (event.target.value || "").toUpperCase()));
        }
    }

    return (
        <div id={"arrow-path-config"} className={"page"}>
            <AdminHeader title={i18n?.arrowPathConfig?.title} />
            <NamesDisplay />
            <BackButton />
            <CompetitorSelection game={"arrowPath"} />
            <div className={"match"}>
                <label htmlFor={"match1"}>
                    {i18n?.arrowPathConfig?.match1}
                </label>
                <input id={"match1"} value={state?.arrowPathConfig?.match1 || ""} onChange={onChange("match1")} />
            </div>
            <div className={"match"}>
                <label htmlFor={"match2"}>
                    {i18n?.arrowPathConfig?.match2}
                </label>
                <input id={"match2"} value={state?.arrowPathConfig?.match2 || ""} onChange={onChange("match2")} />
            </div>
            <div className={"match"}>
                <label htmlFor={"match3"}>
                    {i18n?.arrowPathConfig?.match3}
                </label>
                <input id={"match3"} value={state?.arrowPathConfig?.match3 || ""} onChange={onChange("match3")} />
            </div>
            <div className={"match"}>
                <label htmlFor={"match4"}>
                    {i18n?.arrowPathConfig?.match4}
                </label>
                <input id={"match4"} value={state?.arrowPathConfig?.match4 || ""} onChange={onChange("match4")} />
            </div>
            <div className={"match"}>
                <label htmlFor={"match5"}>
                    {i18n?.arrowPathConfig?.match5}
                </label>
                <input id={"match5"} value={state?.arrowPathConfig?.match5 || ""} onChange={onChange("match5")} />
            </div>
        </div>
    );
};
