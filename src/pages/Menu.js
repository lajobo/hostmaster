import "../style/menu.css";
import {AdminHeader} from "../components/header/Admin";
import {useI18n} from "../hooks/useI18n";
import {MenuButtonGroup} from "../components/menu/ButtonGroup";
import {useContext} from "react";
import {StoreContext} from "../context/store";
import {reset} from "../actions/store";
import {Button} from "../components/button/Button";
import {NamesDisplay} from "../components/names/Display";

export const MenuPage = () => {
    const i18n = useI18n();
    const { dispatch } = useContext(StoreContext);

    return (
        <div id={"menu"} className={"page"}>
            <AdminHeader />
            <div className={"three-column"}>
                <div>
                    <MenuButtonGroup url={"/treasure-map"} title={i18n?.menu?.treasureMap} />
                    <MenuButtonGroup url={"/pigpen-cipher"} title={i18n?.menu?.pigpenCipher} />
                </div>
                <div>
                    <MenuButtonGroup url={"/memory"} title={i18n?.menu?.memory} />
                    <MenuButtonGroup url={"/arrow-path"} title={i18n?.menu?.arrowPath} />
                </div>
                <div>
                    <MenuButtonGroup url={"/music-quiz"} title={i18n?.menu?.musicQuiz} />
                    <MenuButtonGroup url={"/multiple-choice"} title={i18n?.menu?.multipleChoice} />
                </div>
            </div>

            <MenuButtonGroup url={"/scoreboard"} title={i18n?.menu?.scoreboard} i18n={{ play: i18n?.menu?.scoreboardShow }} />
            <Button className={"reset"} onClick={() => {
                dispatch(reset());
            }}>{i18n?.menu?.reset}</Button>
            <NamesDisplay />
        </div>
    );
};
