import "../style/pigpenCipherConfig.css";
import {useI18n} from "../hooks/useI18n";
import {useContext} from "react";
import {StoreContext} from "../context/store";
import {AdminHeader} from "../components/header/Admin";
import {NamesDisplay} from "../components/names/Display";
import {BackButton} from "../components/button/Back";
import {setPigpenCipherConfig} from "../actions/store";
import {CompetitorSelection} from "../components/competitor/Selection";

export const PigpenCipherConfigPage = () => {
    const i18n = useI18n();
    const { state, dispatch } = useContext(StoreContext);

    const onChange = (name) => (event) => {
        dispatch(setPigpenCipherConfig(name, event.target.value));
    }

    return (
            <div id={"pigpen-cipher-config"} className={"page"}>
            <AdminHeader title={i18n?.pigpenCipherConfig?.title} />
            <NamesDisplay />
            <BackButton />
            <CompetitorSelection game={"pigpenCipher"} />
            <div className={"match"}>
                <label htmlFor={"match1"}>
                    {i18n?.pigpenCipherConfig?.match1}
                </label>
                <input id={"match1"} value={state?.pigpenCipherConfig?.match1 || ""} onChange={onChange("match1")} />
            </div>
            <div className={"match"}>
                <label htmlFor={"match2"}>
                    {i18n?.pigpenCipherConfig?.match2}
                </label>
                <input id={"match2"} value={state?.pigpenCipherConfig?.match2 || ""} onChange={onChange("match2")} />
            </div>
            <div className={"match"}>
                <label htmlFor={"match3"}>
                    {i18n?.pigpenCipherConfig?.match3}
                </label>
                <input id={"match3"} value={state?.pigpenCipherConfig?.match3 || ""} onChange={onChange("match3")} />
            </div>
            <div className={"match"}>
                <label htmlFor={"match4"}>
                    {i18n?.pigpenCipherConfig?.match4}
                </label>
                <input id={"match4"} value={state?.pigpenCipherConfig?.match4 || ""} onChange={onChange("match4")} />
            </div>
            <div className={"match"}>
                <label htmlFor={"match5"}>
                    {i18n?.pigpenCipherConfig?.match5}
                </label>
                <input id={"match5"} value={state?.pigpenCipherConfig?.match5 || ""} onChange={onChange("match5")} />
            </div>
        </div>
    );
};
