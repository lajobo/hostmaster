import {AdminHeader} from "../components/header/Admin";
import {NamesDisplay} from "../components/names/Display";
import {BackButton} from "../components/button/Back";
import {CompetitorSelection} from "../components/competitor/Selection";
import {useI18n} from "../hooks/useI18n";

export const MemoryConfigPage = () => {
    const i18n = useI18n();

    return (
        <div id={"memory-config"} className={"page"}>
            <AdminHeader title={i18n?.memoryConfig?.title} />
            <NamesDisplay />
            <BackButton />
            <CompetitorSelection game={"memory"} />
        </div>
    );
};
