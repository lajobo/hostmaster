import "../style/multipleChoiceConfig.css";
import {useI18n} from "../hooks/useI18n";
import {useContext} from "react";
import {StoreContext} from "../context/store";
import {AdminHeader} from "../components/header/Admin";
import {NamesDisplay} from "../components/names/Display";
import {BackButton} from "../components/button/Back";
import {CompetitorSelection} from "../components/competitor/Selection";
import {setMultipleChoiceConfig} from "../actions/store";
import {ConfigDescription} from "../components/config/Description";

export const MultipleChoiceConfigPage = () => {
    const i18n = useI18n();
    const { state, dispatch } = useContext(StoreContext);

    const onChange = (name) => (event) => {
        dispatch(setMultipleChoiceConfig(name, event.target.value));
    }

    return (
        <div id={"multiple-choice-config"} className={"page"}>
            <AdminHeader title={i18n?.multipleChoiceConfig?.title} />
            <NamesDisplay />
            <BackButton />
            <CompetitorSelection game={"multipleChoice"} />
            <ConfigDescription description={i18n?.multipleChoiceConfig?.description} />
            <div className={"two-column"}>
                <div>
                    <div className={"match"}>
                        <label htmlFor={"match1"}>
                            {i18n?.multipleChoiceConfig?.match1}
                        </label>
                        <input id={"match1"} value={state?.multipleChoiceConfig?.match1 || ""} onChange={onChange("match1")} />
                    </div>
                    <div className={"match"}>
                        <label htmlFor={"match2"}>
                            {i18n?.multipleChoiceConfig?.match2}
                        </label>
                        <input id={"match2"} value={state?.multipleChoiceConfig?.match2 || ""} onChange={onChange("match2")} />
                    </div>
                    <div className={"match"}>
                        <label htmlFor={"match3"}>
                            {i18n?.multipleChoiceConfig?.match3}
                        </label>
                        <input id={"match3"} value={state?.multipleChoiceConfig?.match3 || ""} onChange={onChange("match3")} />
                    </div>
                    <div className={"match"}>
                        <label htmlFor={"match4"}>
                            {i18n?.multipleChoiceConfig?.match4}
                        </label>
                        <input id={"match4"} value={state?.multipleChoiceConfig?.match4 || ""} onChange={onChange("match4")} />
                    </div>
                    <div className={"match"}>
                        <label htmlFor={"match5"}>
                            {i18n?.multipleChoiceConfig?.match5}
                        </label>
                        <input id={"match5"} value={state?.multipleChoiceConfig?.match5 || ""} onChange={onChange("match5")} />
                    </div>
                </div>
                <div>
                    <div className={"match"}>
                        <label htmlFor={"match6"}>
                            {i18n?.multipleChoiceConfig?.match6}
                        </label>
                        <input id={"match6"} value={state?.multipleChoiceConfig?.match6 || ""} onChange={onChange("match6")} />
                    </div>
                    <div className={"match"}>
                        <label htmlFor={"match7"}>
                            {i18n?.multipleChoiceConfig?.match7}
                        </label>
                        <input id={"match7"} value={state?.multipleChoiceConfig?.match7 || ""} onChange={onChange("match7")} />
                    </div>
                    <div className={"match"}>
                        <label htmlFor={"match8"}>
                            {i18n?.multipleChoiceConfig?.match8}
                        </label>
                        <input id={"match8"} value={state?.multipleChoiceConfig?.match8 || ""} onChange={onChange("match8")} />
                    </div>
                    <div className={"match"}>
                        <label htmlFor={"match9"}>
                            {i18n?.multipleChoiceConfig?.match9}
                        </label>
                        <input id={"match9"} value={state?.multipleChoiceConfig?.match9 || ""} onChange={onChange("match9")} />
                    </div>
                    <div className={"match"}>
                        <label htmlFor={"match10"}>
                            {i18n?.multipleChoiceConfig?.match10}
                        </label>
                        <input id={"match10"} value={state?.multipleChoiceConfig?.match10 || ""} onChange={onChange("match10")} />
                    </div>
                </div>
            </div>
        </div>
    );
};
