import {AdminHeader} from "../components/header/Admin";
import {useI18n} from "../hooks/useI18n";
import {useContext, useState} from "react";
import {StoreContext} from "../context/store";
import {BackButton} from "../components/button/Back";
import {PointsCounter} from "../components/points/Counter";
import {NextButton} from "../components/button/Next";
import {Navigate} from "react-router";
import {ArrowPathPath} from "../components/arrowPath/Path";
import {ArrowPathSolution} from "../components/arrowPath/Solution";
import {setScore} from "../actions/store";

export const ArrowPathPage = () => {
    const i18n = useI18n();
    const { state, dispatch } = useContext(StoreContext);
    const roundsTotal = Object.keys(state?.arrowPathConfig || {}).length;
    const [roundsCurrent, setRoundsCurrent] = useState(0);
    const text = state?.arrowPathConfig?.[Object.keys((state?.arrowPathConfig || {}))?.[roundsCurrent]];
    const [points, setPoints] = useState([0, 0]);

    return roundsTotal === 0 ? <Navigate to={"config"} /> : (
        <div id={"arrow-path"}  className={"page"}>
            <AdminHeader title={i18n?.arrowPath?.title} />
            <BackButton />
            <NextButton
                roundsTotal={roundsTotal}
                roundsCurrent={roundsCurrent}
                onNextRound={(event) => {
                    if (event.ctrlKey) {
                        setRoundsCurrent((round) => round > 0 ? round - 1 : 0);
                    } else {
                        setRoundsCurrent((round) => (round + 1) < roundsTotal ? round + 1 : (roundsTotal - 1));
                    }


                    if (event.ctrlKey) {
                        setRoundsCurrent(roundsCurrent > 0 ? roundsCurrent - 1 : 0);
                    } else {
                        if ((roundsCurrent + 1) < roundsTotal) {
                            setRoundsCurrent(roundsCurrent + 1);
                        } else {
                            dispatch(setScore(i18n?.arrowPath?.title, points, state?.competitors?.arrowPath))
                        }
                    }
                }}
            />
            <PointsCounter game={"arrowPath"} onPointsChange={setPoints} />
            <ArrowPathSolution text={text} />
            <ArrowPathPath text={text} />
        </div>
    );
};
