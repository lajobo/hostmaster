import "../style/multipleChoiceConfig.css";
import {useI18n} from "../hooks/useI18n";
import {AdminHeader} from "../components/header/Admin";
import {NamesDisplay} from "../components/names/Display";
import {BackButton} from "../components/button/Back";
import {CompetitorSelection} from "../components/competitor/Selection";

export const TreasureMapConfigPage = () => {
    const i18n = useI18n();

    return (
        <div id={"treasure-map-config"} className={"page"}>
            <AdminHeader title={i18n?.treasureMapConfig?.title} />
            <NamesDisplay />
            <BackButton />
            <CompetitorSelection game={"treasureMap"} />
        </div>
    );
};
