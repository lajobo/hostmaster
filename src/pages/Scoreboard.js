import {useContext} from "react";
import {StoreContext} from "../context/store";
import {AdminHeader} from "../components/header/Admin";
import {BackButton} from "../components/button/Back";
import {useI18n} from "../hooks/useI18n";
import {ScoreboardNames} from "../components/scoreboard/Names";
import {ScoreboardGameResult} from "../components/scoreboard/GameResult";
import {ScoreboardTotalResult} from "../components/scoreboard/TotalResult";

export const ScoreboardPage = () => {
    const i18n = useI18n();
    const { state } = useContext(StoreContext);

    return (
        <div className={"page"}>
            <AdminHeader title={i18n?.scoreboard?.title} />
            <BackButton />
            <ScoreboardNames />
            {(state?.score || []).map((score, index) => {
                return (
                    <ScoreboardGameResult
                        key={score?.game + index}
                        id={index + 1}
                        name={score?.game}
                        student={score?.score?.[0]}
                        supervisor={score?.score?.[1]}
                        audience={score?.score?.[2]}
                    />
                );
            })}
            <ScoreboardTotalResult />
        </div>
    );
}
