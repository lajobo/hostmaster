import {AdminHeader} from "../components/header/Admin";
import {useI18n} from "../hooks/useI18n";
import {useContext, useState} from "react";
import {StoreContext} from "../context/store";
import {BackButton} from "../components/button/Back";
import {PointsCounter} from "../components/points/Counter";
import {NextButton} from "../components/button/Next";
import {setScore} from "../actions/store";
import {MemoryGame} from "../components/memory/game";

export const MemoryPage = () => {
    const i18n = useI18n();
    const { state, dispatch } = useContext(StoreContext);
    const [points, setPoints] = useState([0, 0]);

    return (
        <div id={"memory"}  className={"page"}>
            <AdminHeader title={i18n?.memory?.title} />
            <BackButton />
            <NextButton
                roundsTotal={1}
                roundsCurrent={0}
                onNextRound={() => {
                    dispatch(setScore(i18n?.memory?.title, points, state?.competitors?.memory))
                }}
            />
            <PointsCounter game={"memory"} onPointsChange={setPoints} />
            <MemoryGame />
        </div>
    );
};
