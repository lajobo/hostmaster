import {AdminHeader} from "../components/header/Admin";
import {useI18n} from "../hooks/useI18n";
import {useContext, useState} from "react";
import {StoreContext} from "../context/store";
import {BackButton} from "../components/button/Back";
import {PointsCounter} from "../components/points/Counter";
import {PigpenCipherAlphabet} from "../components/pigpenCipher/Alphabet";
import {NextButton} from "../components/button/Next";
import {PigpenCipherGame} from "../components/pigpenCipher/Game";
import {Navigate} from "react-router";
import {setScore} from "../actions/store";

export const PigpenCipherPage = () => {
    const i18n = useI18n();
    const { state, dispatch } = useContext(StoreContext);
    const roundsTotal = Object.keys(state?.pigpenCipherConfig || {}).length;
    const [roundsCurrent, setRoundsCurrent] = useState(0);
    const [points, setPoints] = useState([0, 0]);

    return roundsTotal === 0 ? <Navigate to={"config"} /> : (
        <div id={"pigpen-cipher"}  className={"page"}>
            <AdminHeader title={i18n?.pigpenCipher?.title} />
            <BackButton />
            <NextButton
                roundsTotal={roundsTotal}
                roundsCurrent={roundsCurrent}
                onNextRound={(event) => {
                    if (event.ctrlKey) {
                        setRoundsCurrent(roundsCurrent > 0 ? roundsCurrent - 1 : 0);
                    } else {
                        if ((roundsCurrent + 1) < roundsTotal) {
                            setRoundsCurrent(roundsCurrent + 1);
                        } else {
                            dispatch(setScore(i18n?.pigpenCipher?.title, points, state?.competitors?.pigpenCipher))
                        }
                    }
                }}
            />
            <PointsCounter game={"pigpenCipher"} onPointsChange={setPoints} />
            <PigpenCipherGame roundsCurrent={roundsCurrent} />
            <PigpenCipherAlphabet roundsCurrent={roundsCurrent} />
        </div>
    );
};
