import {useI18n} from "../hooks/useI18n";
import {useContext, useState} from "react";
import {StoreContext} from "../context/store";
import {Navigate} from "react-router";
import {AdminHeader} from "../components/header/Admin";
import {BackButton} from "../components/button/Back";
import {NextButton} from "../components/button/Next";
import {setScore} from "../actions/store";
import {PointsCounter} from "../components/points/Counter";
import {MultipleChoiceGame} from "../components/multipleChoice/Game";

export const MultipleChoicePage = () => {
    const i18n = useI18n();
    const { state, dispatch } = useContext(StoreContext);
    const roundsTotal = Object.keys(state?.multipleChoiceConfig || {}).length;
    const [roundsCurrent, setRoundsCurrent] = useState(0);
    const configCurrent = state?.multipleChoiceConfig?.[Object.keys((state?.multipleChoiceConfig || {}))?.[roundsCurrent]];
    const [points, setPoints] = useState([0, 0]);

    return roundsTotal === 0 ? <Navigate to={"config"} /> : (
        <div id={"multiple-choice"}  className={"page"}>
            <AdminHeader title={i18n?.multipleChoice?.title} />
            <BackButton />
            <NextButton
                roundsTotal={roundsTotal}
                roundsCurrent={roundsCurrent}
                onNextRound={(event) => {
                    if (event.ctrlKey) {
                        setRoundsCurrent(roundsCurrent > 0 ? roundsCurrent - 1 : 0);
                    } else {
                        if ((roundsCurrent + 1) < roundsTotal) {
                            setRoundsCurrent(roundsCurrent + 1);
                        } else {
                            dispatch(setScore(i18n?.multipleChoice?.title, points, state?.competitors?.multipleChoice))
                        }
                    }
                }}
            />
            <PointsCounter
                game={"multipleChoice"}
                value={points}
                onClick={(event, index) => {
                    setPoints((currentPoints) => [
                        currentPoints?.[0] + (index === 0 ? (event.shiftKey ? -1 : 1) : 0),
                        currentPoints?.[1] + (index === 1 ? (event.shiftKey ? -1 : 1) : 0)
                    ])
                }}
            />
            <MultipleChoiceGame
                config={configCurrent}
                onReveal={(revealedPoints) => {
                    setPoints((currentPoints) => [
                        currentPoints?.[0] + revealedPoints?.[0],
                        currentPoints?.[1] + revealedPoints?.[1]
                    ])
                }}
            />
        </div>
    );
};
